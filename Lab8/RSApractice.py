def find_d(e, z):
    for i in range(z):
        if ((i * e) % z) == 1:
            return i


def decrypt(c):
    return (c ** d) % n


def encrypt(m):
    return (m**e) % n


e = 17
p = 211
q = 201

n = p * q
z = (p - 1) * (q - 1)
d = find_d(e, z)

m = 9999


c = encrypt(m)
print(decrypt(c))
