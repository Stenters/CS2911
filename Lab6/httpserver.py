"""
- CS2911 - 0NN
- Fall 2017
- Lab 6
- Names:
  - Stuart Enters
  - Jake Evenson

A simple HTTP server
"""

import socket
import threading
import mimetypes
import os
import datetime
# import re


def main():
    """ Start the server """
    http_server_setup(8080)


def http_server_setup(port):
    """
    Start the HTTP server
    - Open the listening socket
    - Accept connections and spawn processes to handle requests

    :param port: listening port number
    """

    num_connections = 10
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listen_address = ('', port)
    server_socket.bind(listen_address)
    server_socket.listen(num_connections)
    try:
        while True:
            request_socket, request_address = server_socket.accept()
            print('connection from {0} {1}'.format(request_address[0], request_address[1]))
            # Create a new thread, and set up the handle_request method and its argument (in a tuple)
            request_handler = threading.Thread(target=handle_request, args=(request_socket,))
            # Start the request handler thread.
            request_handler.start()
            # Just for information, display the running threads (including this main one)
            print('threads: ', threading.enumerate())
    # Set up so a Ctrl-C should terminate the server; this may have some problems on Windows
    except KeyboardInterrupt:
        print("HTTP server exiting . . .")
        print('threads: ', threading.enumerate())
        server_socket.close()


def handle_request(request_socket):
    """
    Handle a single HTTP request, running on a newly started thread.

    Closes request socket after sending response.
    Should include a response header indicating NO persistent connection
    :param request_socket: socket representing TCP connection from the HTTP client_socket
    :return: None
    """

    try:
        req = get_request(request_socket)
        response = format_response(req[0], req[1])
        print(response)
        if req[1] == "keep-alive":
            request_socket.sendall(response)
        else:
            request_socket.sendall(response)
            request_socket.close()

    except FileNotFoundError:
        request_socket.sendall("HTTP/1.1 404 File Not Found".encode())
    except Exception:
        request_socket.sendall("HTTP/1.1 500 Internal server error".encode())


def get_request(request_socket):
    """
    parses the request and headers
    :return: the request for the file
    :author: Jake
    """
    request = b''
    # Get the request line
    while not request.endswith(b'\r\n\r\n'):
        request += request_socket.recv(1)
    request = request.decode("ASCII")
    request = request.split('\r\n')
    headers = parse_headers(request)

    connection_type = "close"
    if "Connection" in headers:
        connection_type = headers["Connection"]
    request = request[0]
    return request, connection_type


def parse_headers(request):
    """
    reads the headers of the raw request input
    :param request: the raw input of the request
    :return: a dictionary of all the headers of the request
    :author: Jake
    """
    headers = dict()
    for line_number in range(1, len(request) - 2):
        temp = request[line_number].split(': ')
        headers[temp[0]] = temp[1]
    print(headers)
    return headers


def format_response(request_line, connection_type):
    """
    Format the HTTP response to send to the client
    :param request_line: the headers of the HTTP request
    :return: the HTTP response to send to the client
    :author: Stuart
    """
    # Get resource info
    resource = get_resource_request(request_line)
    size = get_file_size(resource)
    mime_type = get_mime_type(resource)

    # Format the header
    msg = ""
    msg += "HTTP/1.1 200 OK\r\n"
    msg += make_header_lines(size, mime_type, connection_type)

    # Turn message into bytes object
    msg = msg.encode()

    # Format the body
    msg += make_body(resource)

    return msg


def get_resource_request(request_line):
    """
    Helper method
    :param request_line:
    :return:
    :author: Stuart
    """
    message = request_line.split(" ")
    resource = message[1]
    if resource == "/":
        resource = "/index.html"
    resource = "." + resource
    if os.path.isfile(resource):
        return resource
    raise FileNotFoundError()


def make_header_lines(size, mime_type, connection_type):
    """
    Helper methods for formatting the required header lines
    :param size: the size of the response body (for Content-Length header)
    :param mime_type: the mime type of the response body
    :return: the header lines of the HTTP response
    :author: Stuart
    """
    # Create the headers
    msg = ""
    message = dict()

    message["Date"] = datetime.datetime.utcnow().strftime("%a, %d %b %Y %H:%M:%S %zGMT")
    message["Server"] = "Pycharm 2018.2.2 (Win64)"
    message["Content-Type"] = mime_type
    message["Content-Length"] = size
    message["Connection"] = connection_type

    for key in message:
        msg += key + ": " + str(message[key]) + "\r\n"
    return msg + "\r\n"


def make_body(resource):
    """
    Helper method for making the body of the response
    :param resource: the resource to send
    :return: the formatted body of the HTTP response
    :author: Stuart
    """
    if os.path.isfile(resource):
        message = b''
        file = open(resource, "rb")
        msg = file.read(1)
        while msg != b'':
            message += msg
            msg = file.read(1)
        return message
    return "Null file"

# ** Do not modify code below this line. You should add additional helper methods above this line. **

# Utility functions
# You may use these functions to simplify your code.


def get_mime_type(file_path):
    """
    Try to guess the MIME type of a file (resource), given its path (primarily its file extension)

    :param file_path: string containing path to (resource) file, such as './abc.html'
    :return: If successful in guessing the MIME type, a string representing the content type, such as 'text/html'
             Otherwise, None
    :rtype: int or None
    """

    mime_type_and_encoding = mimetypes.guess_type(file_path)
    mime_type = mime_type_and_encoding[0]
    return mime_type


def get_file_size(file_path):
    """
    Try to get the size of a file (resource) as number of bytes, given its path

    :param file_path: string containing path to (resource) file, such as './abc.html'
    :return: If file_path designates a normal file, an integer value representing the the file size in bytes
             Otherwise (no such file, or path is not a file), None
    :rtype: int or None
    """

    # Initially, assume file does not exist
    file_size = None
    if os.path.isfile(file_path):
        file_size = os.stat(file_path).st_size
    return file_size


main()
